# Exercice évaluation recrutement R&D ip-label (vue.js)
Ce dépot contient ma solution a l'exercice de recrutement R&D ip-label (vue.js)

## Lancement local
### En développement avec Yarn
Utiliser la commande `yarn serve` dans le dossier `front`
### En production avec Docker et Docker-Compose
Utiliser la commande `docker-compose up -d` dans le dossier racine
## Version en ligne
Une version en ligne est disponible au http://51.75.206.11:8080/
