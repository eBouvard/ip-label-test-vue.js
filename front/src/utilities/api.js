import axios from "axios"

const requestURL = "http://www.omdbapi.com/";
const apiKey = "8f0a5987";
const api = axios.create({
    baseURL: requestURL
})

function buildQuery(queryObject) {
    let query = "?apikey=" + apiKey;
    queryObject.forEach(element => {
        query += ("&" + element.key + "=" + element.value)
    });
    return query;
}

export default (queryObject) => api.get(buildQuery(queryObject));